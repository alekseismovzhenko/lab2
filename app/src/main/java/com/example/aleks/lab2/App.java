package com.example.aleks.lab2;

import android.app.Application;

import com.example.aleks.lab2.dagger.AppComponent;
import com.example.aleks.lab2.dagger.DaggerAppComponent;

/**
 * Created by aleks on 12.12.2017.
 */

public class App extends Application {

    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.create();
    }

}
