package com.example.aleks.lab2.checker;

import com.example.aleks.lab2.commons.PrimeChecker;

/**
 * Created by aleks on 12.12.2017.
 */

public class SimplePrimeCHecker implements PrimeChecker {
    @Override
    public boolean isPrime(int n) {
        if (n % 2 == 0) return false;
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
