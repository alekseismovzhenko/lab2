package com.example.aleks.lab2.commons;

import android.databinding.BindingAdapter;
import android.widget.CheckBox;

/**
 * Created by aleks on 17.12.2017.
 */

public class BindingAdapters {
    @BindingAdapter("onCheckedChange")
    public static void setOnCheckedChangeListener(CheckBox checkBox, OnCheckedChangedListener listener) {
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> listener.checked(isChecked));
    }
}
