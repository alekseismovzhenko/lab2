package com.example.aleks.lab2.commons;

/**
 * Created by aleks on 17.12.2017.
 */

public interface OnCheckedChangedListener {
    void checked(boolean isChecked);
}
