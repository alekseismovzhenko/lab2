package com.example.aleks.lab2.commons;

/**
 * Created by aleks on 12.12.2017.
 */

public interface PrimeGenerator {
    int generatePrime(int maxValue);
}
