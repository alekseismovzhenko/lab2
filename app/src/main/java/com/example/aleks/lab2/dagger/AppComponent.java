package com.example.aleks.lab2.dagger;

import com.example.aleks.lab2.dagger.modules.PrimeCheckModule;
import com.example.aleks.lab2.dagger.modules.PrimeGeneratorModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by aleks on 12.12.2017.
 */
@Singleton
@Component(modules = {PrimeGeneratorModule.class
        , PrimeCheckModule.class})

public interface AppComponent {
}
