package com.example.aleks.lab2.dagger.modules;

import com.example.aleks.lab2.checker.SimplePrimeCHecker;
import com.example.aleks.lab2.commons.PrimeChecker;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by aleks on 12.12.2017.
 */
@Module
public class PrimeCheckModule {
    private PrimeChecker primeChecker = new SimplePrimeCHecker();

    @Provides
    @Singleton
    PrimeChecker providePrimeChecker() {
        return primeChecker;
    }
}
