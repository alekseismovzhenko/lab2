package com.example.aleks.lab2.dagger.modules;

import com.example.aleks.lab2.commons.PrimeGenerator;
import com.example.aleks.lab2.generator.EratosthenesPrimeGenerator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by aleks on 12.12.2017.
 */
@Module
public class PrimeGeneratorModule {
    private PrimeGenerator primeGenerator = new EratosthenesPrimeGenerator();

    @Provides
    @Singleton
    PrimeGenerator providePrimeGenerator() {
        return primeGenerator;
    }
}
