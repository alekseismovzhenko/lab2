package com.example.aleks.lab2.generator;

import com.example.aleks.lab2.commons.PrimeGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by aleks on 12.12.2017.
 */

public class EratosthenesPrimeGenerator implements PrimeGenerator {

    private List<Integer> getPrimeNumbers(int m) {
        List<Integer> primes = new ArrayList<>();
        boolean[] isPrime = new boolean[m];
        Arrays.fill(isPrime, true);
        for (int i = 2; i * i < m; i++)
            if (isPrime[i])
                for (int j = i * i; j < m; j += i)
                    isPrime[j] = false;
        for (int i = 0; i < isPrime.length; i++)
            if (isPrime[i] && i > 1)
                primes.add(i);
        return primes;
    }

    @Override
    public int generatePrime(int maxValue) {
        List<Integer> primes = getPrimeNumbers(maxValue);
        int index = ThreadLocalRandom.current().nextInt(0, primes.size());
        return primes.get(index);
    }
}
