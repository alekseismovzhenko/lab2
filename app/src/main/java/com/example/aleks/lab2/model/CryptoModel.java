package com.example.aleks.lab2.model;

/**
 * Created by aleks on 17.12.2017.
 */

public class CryptoModel {
    public String privateKey;
    public String publickKey;
    public String encrypted;
    public String decrypted;
}
