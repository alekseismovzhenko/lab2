package com.example.aleks.lab2.presentation.presenter.algamal;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.lab2.model.CryptoModel;
import com.example.aleks.lab2.presentation.view.algamal.ElgamalView;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class ElgamalPresenter extends MvpPresenter<ElgamalView> {

    public static final String TAG = ElgamalPresenter.class.getSimpleName();
    private int keySize = 2048;

    public ElgamalPresenter() {

    }

    public void encrypt(String text) {
        Observable.just(text)
                .map(s -> {
                    CryptoModel cryptoModel = new CryptoModel();
                    try {
//                        de.flexiprovider.api.keys.KeyPair keyPair = buildKeyPair();
//                        de.flexiprovider.api.keys.PublicKey publicKey = keyPair.getPublic();
//                        de.flexiprovider.api.keys.PrivateKey privateKey = keyPair.getPrivate();
//
//                        ElGamalPrivateKey gamalPrivateKey= (ElGamalPrivateKey) privateKey;
//                        cryptoModel.privateKey="A="+String.valueOf(gamalPrivateKey.getPublicA());
//                        ElGamalPublicKey gamalPublicKey= (ElGamalPublicKey) publicKey;
//                        cryptoModel.publickKey="A="+String.valueOf(gamalPublicKey.getPublicA());
//                        // encrypt the message
//                        byte[] encrypted = encrypt(gamalPublicKey, text);
//                        cryptoModel.encrypted = new String(encrypted);
//                        // decrypt the message
//                        byte[] secret = decrypt(gamalPrivateKey, encrypted);
//                        cryptoModel.decrypted = new String(secret);
                        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

                        byte[] input = "ab".getBytes();
                        Cipher cipher = Cipher.getInstance("ElGamal/None/NoPadding", "BC");
                        KeyPairGenerator generator = KeyPairGenerator.getInstance("ElGamal", "BC");
                        SecureRandom random = new SecureRandom();

                        generator.initialize(128, random);

                        KeyPair pair = generator.generateKeyPair();
                        Key pubKey = pair.getPublic();
                        Key privKey = pair.getPrivate();
                        cipher.init(Cipher.ENCRYPT_MODE, pubKey, random);
                        byte[] cipherText = cipher.doFinal(input);
                        System.out.println("cipher: " + new String(cipherText));

                        cipher.init(Cipher.DECRYPT_MODE, privKey);
                        byte[] plainText = cipher.doFinal(cipherText);
                        System.out.println("plain : " + new String(plainText));
                        return cryptoModel;
                    } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().setProgress(true))
                .subscribe(cryptoModel -> {
                    getViewState().setModel(cryptoModel);
                    getViewState().setProgress(false);
                });
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }
}
