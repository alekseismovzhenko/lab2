package com.example.aleks.lab2.presentation.presenter.ellipsis;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.lab2.presentation.view.ellipsis.EllipsisView;

@InjectViewState
public class EllipsisPresenter extends MvpPresenter<EllipsisView> {

}
