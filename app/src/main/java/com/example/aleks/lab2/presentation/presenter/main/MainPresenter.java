package com.example.aleks.lab2.presentation.presenter.main;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.lab2.presentation.view.main.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

}
