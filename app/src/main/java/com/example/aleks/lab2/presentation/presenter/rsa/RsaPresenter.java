package com.example.aleks.lab2.presentation.presenter.rsa;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.aleks.lab2.model.CryptoModel;
import com.example.aleks.lab2.presentation.view.rsa.RsaView;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RsaPresenter extends MvpPresenter<RsaView> {

    public static final String TAG = RsaPresenter.class.getSimpleName();
    private int keySize = 2048;

    private byte[] decrypt(PrivateKey privateKey, byte[] encrypted) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(encrypted);
    }

    private byte[] encrypt(PublicKey publicKey, String test) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(test.getBytes());
    }

    private KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);
        return keyPairGenerator.genKeyPair();
    }

    private int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    public void encrypt(String text) {
        Observable.just(text)
                .map(s -> {
                    CryptoModel cryptoModel = new CryptoModel();
                    try {
                        KeyPair keyPair = buildKeyPair();
                        PublicKey pubKey = keyPair.getPublic();
                        PrivateKey privateKey = keyPair.getPrivate();
                        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
                        RSAPublicKey rsaPublicKey = (RSAPublicKey) pubKey;
                        cryptoModel.privateKey = "N=" + String.valueOf(rsaPrivateKey.getModulus()) + " ,d=" + String.valueOf(rsaPrivateKey.getPrivateExponent());
                        cryptoModel.publickKey = "N=" + String.valueOf(rsaPublicKey.getModulus()) + " ,e=" + String.valueOf(rsaPublicKey.getPublicExponent());
                        // encrypt the message
                        byte[] encrypted = encrypt(pubKey, text);
                        cryptoModel.encrypted = new String(encrypted);
                        // decrypt the message
                        byte[] secret = decrypt(privateKey, encrypted);
                        cryptoModel.decrypted = new String(secret);
                        return cryptoModel;
                    } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException e) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().setProgress(true))
                .subscribe(cryptoModel -> {
                    getViewState().setModel(cryptoModel);
                    getViewState().setProgress(false);
                });
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }
}
