package com.example.aleks.lab2.presentation.view.algamal;

import com.arellomobile.mvp.MvpView;
import com.example.aleks.lab2.model.CryptoModel;

public interface ElgamalView extends MvpView {
    void setModel(CryptoModel cryptoModel);

    void setProgress(boolean progress);
}
