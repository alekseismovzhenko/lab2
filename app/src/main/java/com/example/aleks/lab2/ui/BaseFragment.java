package com.example.aleks.lab2.ui;

import com.arellomobile.mvp.MvpAppCompatFragment;

/**
 * Created by aleks on 12.12.2017.
 */

public abstract class BaseFragment extends MvpAppCompatFragment {

    public abstract String getTitle();
}
