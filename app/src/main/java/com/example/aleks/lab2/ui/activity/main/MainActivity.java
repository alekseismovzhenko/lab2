package com.example.aleks.lab2.ui.activity.main;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.lab2.R;
import com.example.aleks.lab2.commons.PagerAdapter;
import com.example.aleks.lab2.databinding.ActivityMainBinding;
import com.example.aleks.lab2.presentation.presenter.main.MainPresenter;
import com.example.aleks.lab2.presentation.view.main.MainView;
import com.example.aleks.lab2.ui.fragment.algamal.ELGamalFragment;
import com.example.aleks.lab2.ui.fragment.ellipsis.EllipsisFragment;
import com.example.aleks.lab2.ui.fragment.rsa.RsaFragment;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    public static final String TAG = "MainActivity";
    @InjectPresenter
    MainPresenter mMainPresenter;
    private ActivityMainBinding binding;
    private PagerAdapter pagerAdapter;

    public static Intent getIntent(final Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(RsaFragment.newInstance());
        pagerAdapter.addFragment(ELGamalFragment.newInstance());
        pagerAdapter.addFragment(EllipsisFragment.newInstance());
        binding.toolbar.setTitle(R.string.lab2);
        binding.viewPager.setAdapter(pagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}
