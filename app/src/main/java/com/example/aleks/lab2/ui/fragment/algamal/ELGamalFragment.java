package com.example.aleks.lab2.ui.fragment.algamal;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.lab2.R;
import com.example.aleks.lab2.commons.OnCheckedChangedListener;
import com.example.aleks.lab2.databinding.FragmentElgamalBinding;
import com.example.aleks.lab2.model.CryptoModel;
import com.example.aleks.lab2.presentation.presenter.algamal.ElgamalPresenter;
import com.example.aleks.lab2.presentation.view.algamal.ElgamalView;
import com.example.aleks.lab2.ui.BaseFragment;

public class ELGamalFragment extends BaseFragment implements ElgamalView, OnCheckedChangedListener {
    public static final String TAG = "ELGamalFragment";
    @InjectPresenter
    public ElgamalPresenter mElgamalPresenter;
    public ObservableBoolean progress = new ObservableBoolean();
    private FragmentElgamalBinding binding;

    public static ELGamalFragment newInstance() {
        ELGamalFragment fragment = new ELGamalFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_elgamal, container, false);
        binding.setFragment(this);
        return binding.getRoot();
    }

    @Override
    public String getTitle() {
        return "ElGamal";
    }

    public void encrypt(View view) {
        mElgamalPresenter.setKeySize(Integer.parseInt(binding.keySizeInput.getEditText().getText().toString()));
        mElgamalPresenter.encrypt(binding.inputET.getText().toString());
    }

    @Override
    public void setModel(CryptoModel cryptoModel) {
        binding.setModel(cryptoModel);
        if (binding.contentLL.getVisibility() != View.VISIBLE) {
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.contentLL.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setProgress(boolean progress) {
        this.progress.set(progress);
    }

    @Override
    public void checked(boolean isChecked) {
        if (isChecked) {
            mElgamalPresenter.setKeySize(2048);
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.keySizeInput.setVisibility(View.GONE);
            binding.keySizeInput.getEditText().setText(String.valueOf(2048));
        } else {
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.keySizeInput.setVisibility(View.VISIBLE);
            binding.keySizeInput.getEditText().setText(String.valueOf(mElgamalPresenter.getKeySize()));
        }
    }
}
