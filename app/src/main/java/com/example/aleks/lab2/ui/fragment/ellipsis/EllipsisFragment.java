package com.example.aleks.lab2.ui.fragment.ellipsis;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.lab2.R;
import com.example.aleks.lab2.databinding.FragmentEllipsisBinding;
import com.example.aleks.lab2.presentation.presenter.ellipsis.EllipsisPresenter;
import com.example.aleks.lab2.presentation.view.ellipsis.EllipsisView;
import com.example.aleks.lab2.ui.BaseFragment;

public class EllipsisFragment extends BaseFragment implements EllipsisView {
    public static final String TAG = "EllipsisFragment";
    @InjectPresenter
    public EllipsisPresenter mEllipsisPresenter;
    private FragmentEllipsisBinding binding;

    public static EllipsisFragment newInstance() {
        EllipsisFragment fragment = new EllipsisFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ellipsis, container, false);
        binding.setFragment(this);
        return binding.getRoot();
    }

    @Override
    public String getTitle() {
        return "Ellipsis";
    }
}
