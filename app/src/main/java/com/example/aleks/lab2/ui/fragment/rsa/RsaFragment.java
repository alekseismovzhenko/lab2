package com.example.aleks.lab2.ui.fragment.rsa;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.aleks.lab2.R;
import com.example.aleks.lab2.commons.OnCheckedChangedListener;
import com.example.aleks.lab2.databinding.FragmentRsaBinding;
import com.example.aleks.lab2.model.CryptoModel;
import com.example.aleks.lab2.presentation.presenter.rsa.RsaPresenter;
import com.example.aleks.lab2.presentation.view.rsa.RsaView;
import com.example.aleks.lab2.ui.BaseFragment;

public class RsaFragment extends BaseFragment implements RsaView, OnCheckedChangedListener {
    public static final String TAG = "RsaFragment";
    @InjectPresenter
    public RsaPresenter mRsaPresenter;
    public ObservableBoolean progress = new ObservableBoolean();
    private FragmentRsaBinding binding;

    public static RsaFragment newInstance() {
        RsaFragment fragment = new RsaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rsa, container, false);
        binding.setFragment(this);
        return binding.getRoot();
    }

    @Override
    public String getTitle() {
        return "RSA";
    }

    public void encrypt(View view) {
        mRsaPresenter.setKeySize(Integer.parseInt(binding.keySizeInput.getEditText().getText().toString()));
        mRsaPresenter.encrypt(binding.inputET.getText().toString());
    }

    @Override
    public void setModel(CryptoModel cryptoModel) {
        binding.setModel(cryptoModel);
        if (binding.contentLL.getVisibility() != View.VISIBLE) {
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.contentLL.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setProgress(boolean progress) {
        this.progress.set(progress);
    }

    @Override
    public void checked(boolean isChecked) {
        if (isChecked) {
            mRsaPresenter.setKeySize(2048);
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.keySizeInput.setVisibility(View.GONE);
            binding.keySizeInput.getEditText().setText(String.valueOf(2048));
        } else {
            TransitionManager.beginDelayedTransition(binding.rootLL);
            binding.keySizeInput.setVisibility(View.VISIBLE);
            binding.keySizeInput.getEditText().setText(String.valueOf(mRsaPresenter.getKeySize()));
        }
    }
}
